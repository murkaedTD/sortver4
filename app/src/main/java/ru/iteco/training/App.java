package ru.iteco.training;

import com.google.common.base.Joiner;

import ru.iteco.training.sort.SortBuilder;
import ru.iteco.training.sort.Sorter;
import ru.iteco.training.sort.strategy.SortStrategyType;
import ru.iteco.training.sort.strategy.pivot.PivotStrategyType;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        String[] rrr = {"3","0","2","1"};
        List<String> sequence = Arrays.asList(rrr);
        Sorter<String> sorter = SortBuilder.newBuilder()
                .sortStrategy(SortStrategyType.QUICK)
                .pivotStrategy(PivotStrategyType.FIRST_ELEMENT)
                .returnOnlyView(true)
                .buildImmutable(new Comparator<String>() {
                    public int compare(String o1, String o2) {
                        int x = Integer.parseInt(o1);
                        int y = Integer.parseInt(o2);

                        if (x == y) return 0;
                        return x > y ? 1 : -1;
                    }
                });

        List<String> result = sorter.sort(sequence);
        System.out.println(Joiner.on(" ").join(result));
    }
}
