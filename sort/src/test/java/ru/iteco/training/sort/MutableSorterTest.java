package ru.iteco.training.sort;

import org.junit.Assert;
import org.junit.Test;
import ru.iteco.training.sort.strategy.BubbleSortStrategy;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class MutableSorterTest {

    @Test
    public void sort() {
        ImmutableSorter immutableSorter = new ImmutableSorter(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return 1;
            }
        }, new BubbleSortStrategy());
        List<String> list = new ArrayList<String>();
        list.add("123");
        list.add("321");
        List<String> list2 = new ArrayList<String>(list);
        immutableSorter.sort(list);
        Assert.assertEquals(list,list2);
//        Assert.assertNotEquals(list,list2);
    }}