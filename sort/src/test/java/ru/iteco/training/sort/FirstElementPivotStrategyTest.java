package ru.iteco.training.sort;

import org.junit.Assert;
import org.junit.Test;
import ru.iteco.training.sort.strategy.pivot.FirstElementPivotStrategy;

import java.util.ArrayList;

public class FirstElementPivotStrategyTest {

    @Test
    public void choose() {
        FirstElementPivotStrategy firstElementPivotStrategy = new FirstElementPivotStrategy();
        ArrayList<String>list=new ArrayList<>();
        int result = firstElementPivotStrategy.choose(list);
        Assert.assertEquals(result,0);
    }
}