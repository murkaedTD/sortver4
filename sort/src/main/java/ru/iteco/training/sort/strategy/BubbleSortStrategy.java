package ru.iteco.training.sort.strategy;

import java.util.Comparator;
import java.util.List;

public class BubbleSortStrategy<T> implements SortStrategy<T> {
    @Override
    public void sort(List<T> objects, Comparator<T> comparator) {
        for (int i = 0; i < objects.size() - 1; i++) {
            for (int j = i; j < objects.size() - 1; j++) {
                if (comparator.compare(objects.get(j+1), objects.get(j)) > 0) {
                    Object temp = objects.get(i);
                    objects.set(i, objects.get(j));
                    objects.set(j, (T) temp);

            }
       }
    }

}}
