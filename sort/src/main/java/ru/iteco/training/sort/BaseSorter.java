package ru.iteco.training.sort;

import ru.iteco.training.sort.strategy.SortStrategy;

import java.util.Comparator;
import java.util.List;

public class BaseSorter<T> implements Sorter<T> {

    private Comparator<T> comparator;
    private SortStrategy<T> strategy;

    public BaseSorter(Comparator<T> comparator, SortStrategy<T> sortStrategy) {
        this.comparator = comparator;
        this.strategy = sortStrategy;
    }



    @Override
    public List<T> sort(List<T> collection) {

        strategy.sort(collection,comparator);
        return collection;
    }
}
