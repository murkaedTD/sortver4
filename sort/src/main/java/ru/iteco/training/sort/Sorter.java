package ru.iteco.training.sort;

import java.util.List;

/**
 *
 * @param <T>
 * сортировка любых обьектов
 */

public  interface Sorter<T> {

    List<T> sort(List<T> collection);

}
