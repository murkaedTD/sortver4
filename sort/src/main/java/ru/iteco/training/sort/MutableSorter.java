package ru.iteco.training.sort;

import ru.iteco.training.sort.strategy.SortStrategy;

import java.util.Comparator;
import java.util.List;

public class MutableSorter<T> extends BaseSorter<T> {
    public MutableSorter(Comparator<T> comparator, SortStrategy<T> sortStrategy) {
        super(comparator, sortStrategy);
    }

    @Override
    public List sort(List<T> collection) {
        return super.sort(collection);
    }
}
