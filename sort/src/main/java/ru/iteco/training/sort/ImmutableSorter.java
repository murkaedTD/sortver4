package ru.iteco.training.sort;

import ru.iteco.training.sort.strategy.SortStrategy;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ImmutableSorter<T> extends BaseSorter<T> {
    public ImmutableSorter(Comparator<T> comparator, SortStrategy<T> sortStrategy) {
        super(comparator, sortStrategy);
    }

    @Override
    public List<T> sort(List<T> collection) {
        List<T> copyList = new ArrayList<T>(collection);
        return super.sort(copyList);
    }
}
