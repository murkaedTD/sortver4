package ru.iteco.training.sort;

import ru.iteco.training.sort.strategy.SortStrategyType;
import ru.iteco.training.sort.strategy.pivot.PivotStrategyType;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * собственно сам билдер
 */

public class SortBuilder {

    /**
     * приватный конструктор для невозможности создания извне
     */
    private SortBuilder() {
    }

    /**
     * переменные необходимые для создания билдера
     */

    private SortStrategyType sortStrategyType;
    private PivotStrategyType pivotStrategyType;
    private Boolean returnOnlyView;

    /**
     * метод создания нового билдера
     */

    public static SortBuilder newBuilder() {
        return new SortBuilder();
    }

    /**
     * метод выбора стратегии сортировки
     */

    public SortBuilder sortStrategy(SortStrategyType sst) {
        sortStrategyType = sst;
        return this;
    }

    /**
     * метод установки пивота(стартового числа)
     */

    public SortBuilder pivotStrategy(PivotStrategyType pst) {
        pivotStrategyType = pst;
        return this;
    }

    /**
     * метод
     */

    public SortBuilder returnOnlyView(Boolean b) {
        returnOnlyView = b;
        return this;
    }

    <T> Sorter<T> Build(final Comparator<T> comparator) {
        if (comparator == null) {
            throw new IllegalArgumentException("undefined comparator");
        }
        if (returnOnlyView) {
            System.out.println("Избыточная конфигурация");
        }
        checkIfAllOk();
        return new MutableSorter<T>(comparator, SortStrategyFactory.<T>getSortStrategy(sortStrategyType, pivotStrategyType));
    }

    public <T> Sorter<T> buildImmutable(final Comparator comparator) {
        if (comparator == null) {
            throw new IllegalArgumentException("undefined comparator");}
            checkIfAllOk();
            return new ImmutableSorter<T>(comparator, SortStrategyFactory.<T>getSortStrategy(sortStrategyType, pivotStrategyType)) {
                @Override
                public List<T> sort(List<T> collection) {
                    List<T> result = super.sort(collection);
                    return returnOnlyView ? Collections.unmodifiableList(result) : result;
                }
            };
        }


    private void checkIfAllOk() {
        if (sortStrategyType == null) {
            throw new IllegalArgumentException("Undefined sort method");
        }
    }
}


