package ru.iteco.training.sort.strategy;

import java.util.Comparator;
import java.util.List;

/**
 * интерфейс сортировки
 *
 *
 * @param <T>
 */
public interface SortStrategy<T> {
    /**
     *
     * @param objects список обьектов для сортировки
     * @param comparator метод сравнения
     */
    void sort(List<T> objects, Comparator<T> comparator);


}
