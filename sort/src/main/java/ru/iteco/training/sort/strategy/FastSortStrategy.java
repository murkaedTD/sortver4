package ru.iteco.training.sort.strategy;

import ru.iteco.training.sort.strategy.pivot.FirstElementPivotStrategy;
import ru.iteco.training.sort.strategy.pivot.PivotStrategy;

import java.util.Comparator;
import java.util.List;

public class FastSortStrategy<T> implements SortStrategy<T> {

    private final PivotStrategy<T> pivot;

    public FastSortStrategy(PivotStrategy<T> pivot) {
        this.pivot = pivot == null ? new FirstElementPivotStrategy<T>() : pivot;
    }

    @Override
    public void sort(List<T> objects, Comparator<T> comparator) {
        if (objects.size() == 0)
            return;
        int pivotInt = pivot.choose(objects);


        int i = 0, j = (objects.size()-1);
        while (i <= j) {
            while (comparator.compare(objects.get(i), objects.get(pivotInt)) < 0) {
                i++;
            }
            while (comparator.compare(objects.get(j), objects.get(pivotInt)) > 0) {
                j--;
            }

            if (i <= j) {
                T temp = objects.get(i);
                objects.set(i, objects.get(j));
                objects.set(j, temp);
                i++;
                j--;
            }
        }





    }}

